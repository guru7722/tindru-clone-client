import React, { useState,useEffect } from 'react';
import TinderCard from 'react-tinder-card';
import '../styles/DatingCard.css';

function DatingCards(props) {
    const [people, setPeople] = useState([]);

    useEffect(()=>{
      props
      .getdata()
      .then((response)=>{
        console.log(response)
        setPeople(response);
      })
      .catch((err)=>{
        console.log(err)
      })
    })
    // swap directions
    const Swipe = (direction) => {
        console.log('You swiped: ' + direction)
      }
      
    // on screen left 
      const onCardLeftScreen = (myIdentifier) => {
        console.log(myIdentifier + ' left the screen')
      }

      // Tinder card maping
      const card = people.map((data)=>{
          const name = data.name
          console.log(data)
        return(
            <TinderCard 
            className='swip'
           onSwipe={(dir)=>Swipe(dir)} 
            onCardLeftScreen={(name) => onCardLeftScreen(name)} 
             preventSwipe={['up', 'down']} >
           <div className='card'>
           <img src={data.imgUrl} alt = "person-img" className='img-1'>
           </img>
           <h3>{data.name}</h3>
           </div>
            </TinderCard>
        )
      })
      
  
    return (<div className="datingCards">
      <div className="datingCards_container">
       {card}
      </div>
    </div>)
    
        
}

export default DatingCards;