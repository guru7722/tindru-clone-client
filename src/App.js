import './App.css';
import DatingCards from './components/DatingCards';
import Header from './components/Hearder';
import SwipButtons from './components/SwipButtons';
import axios from './components/axios';

function App() {

//get req for getiing users data from database
function userdata (){
  return new Promise((resolve,reject) => {
    axios.get("/dating/cards")
    .then((res)=>resolve(res.data))
    .catch((err)=>reject(err.response.data))
  })
}

  return (
    <div className="App">
     <Header />
     <DatingCards getdata={userdata}/>
     <SwipButtons />
    </div>
  );
}

export default App;
