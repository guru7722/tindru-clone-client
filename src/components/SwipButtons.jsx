import React from 'react';
import '../styles/SwipBtns.css';
import IconButton from '@material-ui/core/IconButton';
import ReplayIcon from '@mui/icons-material/Replay';
import CloseIcon from '@mui/icons-material/Close';
import StarRateIcon from '@mui/icons-material/StarRate';
import FavoriteIcon from '@mui/icons-material/Favorite';
import FlashOnIcon from '@mui/icons-material/FlashOn';
function SwipButtons() {
  return (
    <div className='swip_btns'>
        <IconButton className='swip_replay'><ReplayIcon fontSize='large' /></IconButton>
        <IconButton className='swip_close'><CloseIcon fontSize='large' /></IconButton>
        <IconButton className='swip_star'><StarRateIcon fontSize='large' /></IconButton>
        <IconButton className='swip_favrt'><FavoriteIcon fontSize='large' /></IconButton>
        <IconButton className='swip_flash_On'><FlashOnIcon fontSize='large' /></IconButton>
    </div>
    
  )
}


export default SwipButtons