import React from 'react';
import '../styles/Header.css';
import PersonIcon from '@mui/icons-material/Person';
import IconButton from '@material-ui/core/IconButton';
import ForumIcon from '@mui/icons-material/Forum';
import logo from '../imgs/Group 1.png';
function Hearder
() {
  return (

    <div className='header_icons'>
<IconButton >
<PersonIcon fontSize="large" className="header_icon" />
</IconButton>
<img className="header_logo" src={logo}/>
<IconButton>
<ForumIcon fontSize="large" className="header_icon" />
</IconButton>
    </div>
  )
}

export default Hearder